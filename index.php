<?php

$search_words = array('php', 'html', 'интернет', 'Web');

$search_strings = array(
'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
'PHP - это распространенный язык программирования с открытым исходным кодом.',
'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
);

function findWords($search_words, $search_strings){
    for ($i=0; $i < count($search_strings); $i++)
    {
        $result = [];
        foreach ($search_words as $search_word)
        {
            $reg = '/' . $search_word . '/iu';
            if (preg_match($reg, $search_strings[$i])) {
                $result[] = $search_word;
            }
        }
        echo 'В предложении' . ' ' . ($i+1) . ' ' . 'есть слова:' . ' '. implode(', ', $result) . '<br>';
    }
}

findWords($search_words, $search_strings);